package com.scala.retail.worldbank

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import java.lang.Double
import org.apache.spark.sql.SparkSession
import scala.xml.XML

/**
 * input: 2012-01-01	09:00	San Jose	Men's Clothing	214.05	Amex
 */
object RetailKPI1 {
  def main(args: Array[String]) = {
    //get Spark conf
    //val conf = new SparkConf().setAppName("Retail Analysis KPI1");
    //get Spark Context
    //val context = new SparkContext(conf)
    //spark context is old
    
    val spark = SparkSession.builder().appName("World Bank").getOrCreate()
    val lines = spark.read.csv(args(0)).rdd
    
    //10th elem
    lines.map{ line => {
      val population = line.getString(10).replaceAll(",", "")
      
    }

    }
    
    //check length of arguments
    if (args.length != 2) {
      println("Must input source and destination directories")
      System.exit(1)
    }

    //get the input and create RDD
    val retailRDD = context.textFile(args(0))

    //count of sales
    retailRDD.count()
    //split each line and return key value pair
    val tuple:RDD[(String, Double)] = retailRDD.map{ l => { val tokens = l.split("\t")
      (tokens(3),  Double.valueOf(tokens(4)))
    }}
    //val tuple = retailRDD.map(l => (l.split("\t")(3),  (l.split("\t")(4))))
    
    //calculate sales total 
    val productSales = tuple.reduceByKey((x, y)=> x + y)
    
    productSales.saveAsTextFile(args(1))
    
    context.stop()
  }
}