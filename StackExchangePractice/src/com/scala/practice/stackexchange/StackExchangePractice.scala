package com.scala.practice.stackexchange

import org.apache.spark.sql.SparkSession
import scala.xml.XML
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.time.LocalDate
import org.joda.time.format.DateTimeFormatter

object StackExchangePractice {
  def main(args: Array[String]) = {
    //set
    System.setProperty("hadoop.home.dir", "C:\\Hadoop\\Software\\hadoop-2.5.0-cdh5.3.2")
    System.setProperty("spark.sql.warehouse.dir", "file:/C:/Spark/spark-2.0.2-bin-hadoop2.6/spark-warehouse")

    //load HDD
    val session = SparkSession.builder.appName("Stack Exchange KPI").master("local").getOrCreate()

    val data = session.read.textFile("C:\\Spark\\Assignment\\Posts.xml").rdd
    val df = session.read.csv("C:\\Spark\\Assignment\\Posts.xml")
    

    /*var resultKPI2 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .flatMap { line =>
        {
          val xml = XML.loadString(line)
          xml.attribute("CreationDate")
        }
      }
      .map { line => { (line.mkString.substring(0, 7), 1) } }
      .reduceByKey((x, y) => x + y)
    resultKPI2.foreach(println)*/

    var resultKPI4 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line =>
        {
          val xml = XML.loadString(line)
          val body = xml.attributes("Body")
          body.mkString.contains("Hadoop")
        }
      }
      .map { line =>
        {
          val xml = XML.loadString(line)
          val body = xml.attributes("Body").mkString.replaceAll("\n", " ")
          val id = xml.attributes("Id").mkString
          val viewCount = Integer.parseInt(xml.attributes("ViewCount").mkString)
          (body, viewCount)
        }
      }.sortBy(_._2, false)
    //.collect()
    //.take(10)

    //resultKPI4.foreach(println)

    //Questions with no answer
    var resultKPI5 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line =>
        {
          val xml = XML.loadString(line)
          val answerCount = Integer.parseInt(xml.attribute("AnswerCount").getOrElse(0).toString())
          answerCount.equals(0)
        }
      }

    //println(resultKPI5.count())

    //Number of questions with more than 2 answers
    var resultKPI6 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line =>
        {
          val xml = XML.loadString(line)
          val answerCount = Integer.parseInt(xml.attribute("AnswerCount").getOrElse(0).toString())
          answerCount > 2
        }
      }

    //println(resultKPI6.count())

    //Number of questions which are active for last 6 months
    var resultKPI7 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line =>
        {
          val xml = XML.loadString(line)
          val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
          val date = df.parse(xml.attribute("CreationDate").mkString)
          //write some lofgic to calculate six months
          1 == 1
        }
      }

    var resultKPI8 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line => { line.contains("ClosedDate") } }
      .flatMap { line =>
        {
          val xml = XML.loadString(line)
          xml.attribute("ClosedDate")
        }
      }
      .map { line => { (line.mkString.substring(0, 7), 1) } }
      .reduceByKey(_ + _)
    //resultKPI8.foreach(println)

    var resultKPI9 = data.filter { line => { line.trim().startsWith("<row") } }
      .filter { line => { line.contains("PostTypeId=\"1\"") } }
      .filter { line =>
        {
          val xml = XML.loadString(line)
          val body = xml.attributes("Body")
          body.mkString.contains("Hadoop")
        }
      }
      .map { line =>
        {
          val xml = XML.loadString(line)
          val body = xml.attributes("Body").mkString.replaceAll("\n", " ")
          val id = xml.attributes("Id").mkString
          val viewCount = Integer.parseInt(xml.attributes("ViewCount").mkString)
          (body, viewCount)
        }
      }.sortBy(_._2, false)
      .reduceByKey(_ + _)
    //resultKPI9.foreach(println)

    //KPI12
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    val format2 = new SimpleDateFormat("yyyy-MM");

    val baseData = data.filter { line => { line.trim().startsWith("<row") }
    }
      .map { line =>
        {
          val xml = XML.loadString(line)
          var aaId = "";
          if (xml.attribute("AcceptedAnswerId") != None) {
            aaId = xml.attribute("AcceptedAnswerId").get.toString()
          }
          val crDate = xml.attribute("CreationDate").get.toString()
          val rId = xml.attribute("Id").get.toString()
          //			  (closeDate, line)
          (rId, aaId, crDate)
        }
      }
    baseData.foreach(println)
    //(10469,10472,2016-03-01T21:26:04.460)
    val aaData = baseData.map { data =>
      {
        (data._2, data._3)
      }
    }
      .filter { data => { data._1.length() > 0 } }

      aaData.foreach(println)

    val rdata = baseData.map { data =>
      {
        (data._1, data._3)
      }
    }

    rdata.foreach(println)
    val joinData = rdata.join(aaData)
      .map { data =>
        {
          val quesDate = format.parse(data._2._2).getTime
          val ansDate = format.parse(data._2._1).getTime
          val diff: Float = ansDate - quesDate
          val time: Float = diff / (1000 * 60 * 60) //millisecond to hour
          //			  (data, time)
          time
        }
      }
    val count = joinData.count()
    val result = joinData.sum() / count

    //println(result)
    session.stop()
  }
}