package com.spark.retail;

import scala.Tuple1;
import scala.Tuple2;
import scala.Tuple3;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public final class RetailDataAnalysis {
	private static final Pattern SPACE = Pattern.compile(" ");

	public static void main(String[] args) throws Exception {

		if (args.length < 2) {
			System.err.println("Usage: Retail Data <Input-File> <Output-File>");
			System.exit(1);
		}

		SparkSession spark = SparkSession.builder().appName("Retail Data").getOrCreate();

		JavaRDD<String> lines = spark.read().textFile(args[0]).javaRDD();
		
		PairFunction<String, String, Float> p = new PairFunction<String, String, Float>() {
			@Override
			public Tuple2<String, Float> call(String s) {
				String[] str = s.split("\t");
				return new Tuple2<>(str[2], Float.valueOf(str[4]));
			}
		};

		JavaPairRDD<String, Float> salesByStore = lines.mapToPair(p);

		JavaPairRDD<String, String> sales = salesByStore.reduceByKey(new Function2<Float, Float, String>() {
			@Override
			public String call(Float f1, Float f2) {
				return String.valueOf(f1 + f2)+String.valueOf(1+1);
			}
		});

		sales.saveAsTextFile(args[1]);
		spark.stop();
	}
}
// bin/spark-submit --class com.dataflair.wc.JavaWordCount ../sparkJob.jar
// ../about-dataflair.txt wc-out-01