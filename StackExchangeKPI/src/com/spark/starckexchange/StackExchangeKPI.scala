
package com.spark.starckexchange

import org.apache.spark.sql.SparkSession
import scala.xml.XML

object StackExchangeKPI {

  def main(args: Array[String]) = {
    System.setProperty("hadoop.home.dir", "C:\\Hadoop\\Software\\hadoop-2.5.0-cdh5.3.2")
    System.setProperty("spark.sql.warehouse.dir", "file:/C:/Spark/spark-2.0.2-bin-hadoop2.6/spark-warehouse")

    val spark = SparkSession
      .builder
      .appName("AvgAnsTime")
      .master("local")
      .getOrCreate()

    val data = spark.read.textFile("C:\\Spark\\Assignment\\Posts.xml").rdd

			val result = data.filter{line => {line.trim().startsWith("<row")}
			}
			.filter { line => {line.contains("PostTypeId=\"1\"")}
			}
			.map {line => {
			  val xml = XML.loadString(line)
			  xml.attribute("Title")
			  }
			}
			.filter { line => {
			  line.mkString.toLowerCase().contains("hadoop")
			}
			}
			
			result.foreach { println }
			println ("Result Count: " + result.count())

    spark.stop
  }
}